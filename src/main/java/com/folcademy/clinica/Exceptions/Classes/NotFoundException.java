package com.folcademy.clinica.Exceptions.Classes;

public class NotFoundException extends RuntimeException{
    public NotFoundException(String message){
        super(message);
    }
}
