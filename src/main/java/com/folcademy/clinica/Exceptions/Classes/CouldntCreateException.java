package com.folcademy.clinica.Exceptions.Classes;

public class CouldntCreateException extends RuntimeException{
    public CouldntCreateException(String message) {
        super(message);
    }
}
