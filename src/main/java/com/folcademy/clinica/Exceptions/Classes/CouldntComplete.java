package com.folcademy.clinica.Exceptions.Classes;

public class CouldntComplete extends RuntimeException{
    public CouldntComplete(String message) {
        super(message);
    }
}
