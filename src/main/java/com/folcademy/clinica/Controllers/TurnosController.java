package com.folcademy.clinica.Controllers;

import com.folcademy.clinica.Exceptions.Classes.NotFoundException;
import com.folcademy.clinica.Exceptions.Classes.ValidationException;
import com.folcademy.clinica.Model.Dtos.TurnoDto;
import com.folcademy.clinica.Services.TurnoService;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/turnos")
public class TurnosController {
    private final TurnoService turnoService;

    public TurnosController(TurnoService turnoService) {
        this.turnoService = turnoService;
    }

//    @PreAuthorize("hasAuthority('turnos_findAll')")
    @GetMapping("")
    public ResponseEntity<List<TurnoDto>> findAll(){
        return ResponseEntity.ok(turnoService.findAll());
    }

//    @PreAuthorize("hasAuthority('turnos_findById')")
    @GetMapping("/page")
    public ResponseEntity<Page<TurnoDto>> findAll(
        @RequestParam(name="pageNumber", defaultValue = "0") Integer pageNumber,
        @RequestParam(name="pageSize", defaultValue = "4") Integer pageSize,
        @RequestParam(name="orderField", defaultValue = "fecha") String sortBy)
    {
        if(pageNumber<0)
            throw new ValidationException("Valor invalido. Numero de página negativo.");
        if(pageSize<0)
            throw new ValidationException("Valor invalido. Tamaño de página negativo.");

        return ResponseEntity.ok(turnoService.findAll(pageNumber,pageSize,sortBy));
    }

//    @PreAuthorize("hasAuthority('turnos_findById')")
    @GetMapping("/{id}")
    public ResponseEntity<TurnoDto> findById(@PathVariable(name="id") Integer id){
        return ResponseEntity.ok(turnoService.findById(id));
    }

//    @PreAuthorize("hasAuthority('turnos_addOne')")
    @PostMapping("")
    public ResponseEntity<TurnoDto> addOne(@RequestBody @Validated TurnoDto turno){
        if(turno.getMedico()==null || turno.getMedico().getId()==null)
            throw new ValidationException("Medico no valido");

        if(turno.getPaciente()==null || turno.getPaciente().getId()==null)
            throw new ValidationException("Paciente no valido");

        TurnoDto saved = turnoService.save(turno);
        if(saved==null)
            throw new ValidationException("Error de datos.");

        return ResponseEntity.ok(saved);
    }

//    @PreAuthorize("hasAuthority('turnos_editOne')")
    @PutMapping(value="/{id}")
    public ResponseEntity<TurnoDto> editOne(@PathVariable(name="id") Integer id,
                                            @RequestBody TurnoDto t){
        t.setId(id);
        TurnoDto editted = turnoService.edit(t);
        return ResponseEntity.ok(editted);
    }

//    @PreAuthorize("hasAuthority('turnos_delete')")
    @DeleteMapping(value="/{id}")
    public ResponseEntity<TurnoDto> delete(@PathVariable(name="id") int id) {
        TurnoDto turno = turnoService.delete(id);

        if(turno==null)
            throw new NotFoundException("Turno no encontrado");

        return ResponseEntity.ok(turno);
    }
}
