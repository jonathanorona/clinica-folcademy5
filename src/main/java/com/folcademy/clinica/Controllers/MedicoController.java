package com.folcademy.clinica.Controllers;

import com.folcademy.clinica.Exceptions.Classes.AlreadyExistsException;
import com.folcademy.clinica.Exceptions.Classes.CouldntComplete;
import com.folcademy.clinica.Exceptions.Classes.NotFoundException;
import com.folcademy.clinica.Exceptions.Classes.ValidationException;
import com.folcademy.clinica.Model.Dtos.MedicoDto;
import com.folcademy.clinica.Services.MedicoService;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/medicos")
public class MedicoController {
    private final MedicoService medicoService;

    public MedicoController(MedicoService medicoService) {
        this.medicoService = medicoService;
    }

//    @PreAuthorize("hasAuthority('turnos_findAll')")
    @GetMapping(value="")
    public ResponseEntity<List<MedicoDto>> findAll(){
        return ResponseEntity.ok(medicoService.findAll());
    }

//    @PreAuthorize("hasAuthority('turnos_findById')")
    @GetMapping("/page")
    public ResponseEntity<Page<MedicoDto>> findAll(
            @RequestParam(name="pageNumber", defaultValue = "0") Integer pageNumber,
            @RequestParam(name="pageSize", defaultValue = "4") Integer pageSize,
            @RequestParam(name="orderField", defaultValue = "apellido") String sortBy
    ){
        if(pageNumber<0)
            throw new ValidationException("Valor invalido. Numero de página negativo.");
        if(pageSize<0)
            throw new ValidationException("Valor invalido. Tamaño de página negativo.");

        return ResponseEntity.ok(medicoService.findAll(pageNumber,pageSize,sortBy));
    }

    @GetMapping(value="/{id}")
    public ResponseEntity<MedicoDto> findById(@PathVariable(name="id") Integer id){
        MedicoDto m = medicoService.findById(id);
        if(m==null)
            throw new NotFoundException("Medico no encontrado.");

        return ResponseEntity.ok(medicoService.findById(id));
    }

//    @PreAuthorize("hasAuthority('turnos_addOne')")
    @PostMapping
    public ResponseEntity<MedicoDto> addOne(@RequestBody @Validated MedicoDto dto){
        if(medicoService.alreadyExists(dto))
            throw new AlreadyExistsException("Ya existe. Medico no guardado.");

        if(dto.getConsulta()<0)
            throw new ValidationException("Consulta con valor no valido (Valor negativo).");

        MedicoDto medico = medicoService.save(dto);
        if(medico==null)
            throw new CouldntComplete("No se logro completar la operacion.");

        return ResponseEntity.ok(medico);
    }


//    @PreAuthorize("hasAuthority('turnos_editOne')")
    @PutMapping(value = "/{id}")
    public ResponseEntity<MedicoDto> editOne(@PathVariable(name="id") Integer id,
                                             @RequestBody @Validated MedicoDto dto){
        MedicoDto m = medicoService.findById(id);
        if(m==null)
            throw new NotFoundException("No se encontró el médico");

        if(dto.getConsulta()<0)
            throw new ValidationException("Consulta con valor negativo.");

        dto.setId(id);
        return ResponseEntity.ok(medicoService.edit(dto));
    }


//    @PreAuthorize("hasAuthority('turnos_delete')")
    @DeleteMapping(value="/{id}")
    public ResponseEntity<MedicoDto> delete(@PathVariable(name="id")Integer id){
        MedicoDto m = medicoService.findById(id);
        if(m==null)
            throw new NotFoundException("Medico no encontrado. No se elimino ningun medico.");

        if(medicoService.delete(m.getId())==null)
            throw new CouldntComplete("La operacion falló. No se elimino ningun médico.");

        return ResponseEntity.ok(m);
    }

}
