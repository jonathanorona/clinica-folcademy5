package com.folcademy.clinica.Model.Entities;

import com.folcademy.clinica.Model.Dtos.MedicoDto;
import com.folcademy.clinica.Model.Dtos.PacienteDto;
import lombok.*;
import org.hibernate.Hibernate;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Objects;

/*@Table(name = "turno", indexes = {
        @Index(name = "fk_paciente_idx", columnList = "idpaciente"),
        @Index(name = "fk_medico_idx", columnList = "idmedico")
})*/
@Table(name="turno")
@Entity
@Getter
@Setter
@ToString
@RequiredArgsConstructor
public class Turno {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idturno", columnDefinition = "INTEGER")
    private Integer id;

    @Column(name = "fecha", nullable = false, columnDefinition = "date")
    private LocalDate fecha;

    @Column(name = "hora", nullable = false, columnDefinition = "time(2)")
    private LocalTime hora;

    @Column(name = "atendido",columnDefinition = "boolean")
    private Boolean atendido=false;

    @ManyToOne(cascade = CascadeType.REMOVE)
    @JoinColumn(name = "idpaciente", referencedColumnName = "idpaciente")
//    @NotFound(action = NotFoundAction.EXCEPTION)
    private Paciente paciente;

    @ManyToOne(cascade = CascadeType.REMOVE)
//    @NotFound(action = NotFoundAction.EXCEPTION)
    @JoinColumn(name = "idmedico", referencedColumnName = "idmedico")
    private Medico medico;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Turno turno = (Turno) o;
        return id != null && Objects.equals(id, turno.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}