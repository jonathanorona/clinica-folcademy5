package com.folcademy.clinica.Model.Entities;

import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "paciente")
@Getter
@Setter
@ToString
@RequiredArgsConstructor
public class Paciente {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idpaciente", columnDefinition = "INTEGER")
    public Integer id;
    @Column(name = "dni", columnDefinition = "VARCHAR",nullable = false)
    public String dni;
    @Column(name = "Nombre", columnDefinition = "VARCHAR")
    public String nombre;
    @Column(name = "Apellido", columnDefinition = "VARCHAR",nullable = false)
    public String apellido;
    @Column(name = "Telefono", columnDefinition = "VARCHAR")
    public String telefono;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Paciente paciente = (Paciente) o;
        return id != null && Objects.equals(id, paciente.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
