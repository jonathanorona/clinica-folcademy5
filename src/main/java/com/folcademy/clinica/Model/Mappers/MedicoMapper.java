package com.folcademy.clinica.Model.Mappers;

import com.folcademy.clinica.Model.Dtos.MedicoDto;
import com.folcademy.clinica.Model.Entities.Medico;
import org.springframework.stereotype.Component;

import java.util.Optional;


@Component
public class MedicoMapper {
    public MedicoDto entityToDto(Medico entity){
        return Optional
                .ofNullable(entity)
                .map(e -> new MedicoDto(
                        e.getId(),
                        e.getProfesion(),
                        e.getConsulta(),
                        e.getNombre(),
                        e.getApellido()
                ))
                .orElse(new MedicoDto());
    }

    public Medico dtoToEntity(MedicoDto dto){
        Medico ent = new Medico();
        ent.setId(dto.getId());
        ent.setProfesion(dto.getProfesion());
        ent.setConsulta(dto.getConsulta());
        ent.setNombre(dto.getNombre());
        ent.setApellido(dto.getApellido());
        return ent;
    }
}
