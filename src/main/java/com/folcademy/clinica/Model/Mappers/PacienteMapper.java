package com.folcademy.clinica.Model.Mappers;

import com.folcademy.clinica.Model.Dtos.PacienteDto;
import com.folcademy.clinica.Model.Entities.Medico;
import com.folcademy.clinica.Model.Entities.Paciente;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class PacienteMapper {
    public PacienteDto entityDto(Paciente entity){
        return Optional.ofNullable(entity)
                .map(e-> new PacienteDto(
                        e.getId(),
                        e.getNombre(),
                        e.getApellido(),
                        e.getDni()
                ))
                .orElse(new PacienteDto());
    }

    public Paciente dtoToEntity(PacienteDto dto){
        Paciente ent = new Paciente();
        ent.setId(dto.getId());
        ent.setNombre(dto.getNombre());
        ent.setApellido(dto.getApellido());
        ent.setDni(dto.getDni());
        return ent;
    }

}
