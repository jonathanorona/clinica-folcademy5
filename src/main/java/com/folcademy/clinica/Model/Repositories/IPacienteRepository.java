package com.folcademy.clinica.Model.Repositories;

import com.folcademy.clinica.Model.Entities.Paciente;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface IPacienteRepository extends PagingAndSortingRepository<Paciente, Integer> {
    List<Paciente> findAllByApellido(String apellido);
    Optional<Paciente> findById(Integer id);
    Optional<Paciente> findByDni(String dni);
    Optional<Paciente> findByNombreLikeAndApellidoLike(String nombre, String apellido);

    Page<Paciente> findAll(Pageable pageable);

}
