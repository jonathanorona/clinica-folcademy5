package com.folcademy.clinica.Services;

import com.folcademy.clinica.Exceptions.Classes.NotFoundException;
import com.folcademy.clinica.Exceptions.Classes.ValidationException;
import com.folcademy.clinica.Model.Dtos.PacienteDto;
import com.folcademy.clinica.Model.Dtos.TurnoDto;
import com.folcademy.clinica.Model.Entities.Medico;
import com.folcademy.clinica.Model.Entities.Paciente;
import com.folcademy.clinica.Model.Entities.Turno;
import com.folcademy.clinica.Model.Mappers.TurnoMapper;
import com.folcademy.clinica.Model.Repositories.IMedicoRepository;
import com.folcademy.clinica.Model.Repositories.IPacienteRepository;
import com.folcademy.clinica.Model.Repositories.ITurnoRepository;
import com.folcademy.clinica.Services.Interfaces.ITurnoService;
import com.sun.istack.NotNull;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class TurnoService implements ITurnoService {
    private final ITurnoRepository turnoRepository;
    private final TurnoMapper turnoMapper;
    private final IPacienteRepository pacienteRepository;
    private final IMedicoRepository medicoRepository;

    public TurnoService(ITurnoRepository turnoRepository, TurnoMapper turnoMapper, IPacienteRepository pacienteRepository, IMedicoRepository medicoRepository) {
        this.turnoRepository = turnoRepository;
        this.turnoMapper = turnoMapper;
        this.pacienteRepository = pacienteRepository;
        this.medicoRepository = medicoRepository;
    }

    @Override
    public List<TurnoDto> findAll() {
        return turnoRepository
                .findAll()
                .stream()
                .map(turnoMapper::entityToDto)
                .collect(Collectors.toList());
    }

    public Page<TurnoDto> findAll(Integer pageNumber,Integer pageSize,String sortBy){
        Pageable pageable = PageRequest.of(pageNumber,pageSize, Sort.by(sortBy));
        return turnoRepository.findAll(pageable).map(turnoMapper::entityToDto);
    }

    @Override
    public TurnoDto findById(Integer id) {
        return turnoRepository
                .findById(id)
                .map(turnoMapper::entityToDto)
                .orElse(null);
    }

    public TurnoDto save(TurnoDto e) {
        if(!medicoRepository.findById(e.getMedico().getId()).isPresent()
                || !pacienteRepository.findById(e.getPaciente().getId()).isPresent())
            return null;

        return turnoMapper.entityToDto(turnoRepository.save(turnoMapper.dtoToEntity(e)));
    }

    public TurnoDto edit(TurnoDto dto) {
        if(dto.getId()==null ||dto.getPaciente().getId()==null || dto.getMedico().getId()==null )
            throw new ValidationException("Datos no validos.");

        if(!turnoRepository.findById(dto.getId()).isPresent())
            throw new NotFoundException("Turno no encontrado.");

        if( !pacienteRepository.findById(dto.getPaciente().getId()).isPresent())
            throw new NotFoundException("Paciente no encontrado.");

        if(!medicoRepository.findById(dto.getMedico().getId()).isPresent())
            throw new NotFoundException("Medico no encontrado.");

        return turnoMapper.entityToDto(turnoRepository.save(turnoMapper.dtoToEntity(dto)));
    }

    public TurnoDto delete(Integer id) {
        Optional<Turno> deleteable = turnoRepository.findById(id);
        if(!deleteable.isPresent())
            return null;
        else {
            turnoRepository.deleteById(id);
            return turnoMapper.entityToDto(deleteable.get());
        }
    }
}
