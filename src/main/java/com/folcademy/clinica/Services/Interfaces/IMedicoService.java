package com.folcademy.clinica.Services.Interfaces;

import com.folcademy.clinica.Model.Dtos.MedicoDto;
import com.folcademy.clinica.Model.Entities.Medico;

import java.util.List;

public interface IMedicoService {
    List<MedicoDto> findAll();
    MedicoDto findById(Integer id);
}
