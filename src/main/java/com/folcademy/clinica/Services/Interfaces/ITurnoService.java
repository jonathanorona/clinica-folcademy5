package com.folcademy.clinica.Services.Interfaces;

import com.folcademy.clinica.Model.Dtos.TurnoDto;
import com.folcademy.clinica.Model.Entities.Turno;

import java.util.List;

public interface ITurnoService {
    List<TurnoDto> findAll();
    TurnoDto findById(Integer id);
}
